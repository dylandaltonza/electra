import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm = this.fb.group({
    username: ["", [ Validators.required, Validators.minLength(3) ] ],
    password: ["", Validators.required],
  })

  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
  }

  clear(){
    this.loginForm.reset();
  }

  onSubmit(){
    console.log(this.loginForm.value);
  }

}
